//: Playground - noun: a place where people can play

import UIKit

let name = "Rosa"
let personalizedGreeting = "Welcome, \(name)!"


let price = 2
let number = 3
let cookiePrice = "\(number) cookies: $\(price * number). e\u{301}"


let coco = "💩 control+command+space"
let 💩 = "COCO"

let number1:Double = 10
let number2:Double = 2.5
let result:Double = number1/number2


var student:(name:String, age:Int) = ("Luiza Prata", 31)
student.name = "Lulu"
student.age
student

var grades:[Double] = [10.0, 1.5, 8]
grades.append(10);
var total:Double = 0;
for grade in grades {
    total += grade
}
let average = total/Double(grades.count)

let firstLetter = ["Lucas", "Luiza", "Joca"]


let digitNames = [
    0: "Zero", 1: "One", 2: "Two",   3: "Three", 4: "Four",
    5: "Five", 6: "Six", 7: "Seven", 8: "Eight", 9: "Nine"
]

let numbers = [16, 58, 84]
let strings = numbers.map {
    (number) -> String in
    var number = number
    var output = ""
    repeat {
        output = digitNames[number % 10]! + output
        number /= 10
    } while number > 0
    return output
}

let squares2 = numbers.map({
    (value: Int) -> Int in
    return value * value
})


//Optional binding
let test:Int?
test = 75
print(test!) //unwrap vida loca
if let test2 = test{ //unwrap certo
    print(test2);
}
if (test != nil){ //ok tbm mas acima eh mais comum
    print(test!)
}

class Person{
    var name: String
    var age: Int
    init(name:String, age:Int) {
        self.name = name
        self.age = age
    }
    func sayWho() -> String {
        return "Meu nome é \(self.name) e tenho \(self.age) anos."
    }
}

let person:Person? = Person.init(name:"Luiza", age:31)
print(person?.sayWho())